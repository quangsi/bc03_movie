import { userService } from "../../services/userService";
import { SET_USER_INFOR } from "../constants/userConstants";

export const setUserInforAction = (user) => {
  return {
    type: SET_USER_INFOR,
    payload: user,
  };
};
// let handleLoginSuccess = (data) => {
//   return dispatch({
//     type: SET_USER_INFOR,
//     payload: data,
//   });};

let handleLoginFail = () => {};

export const setUserInforActionService = (
  dataLogin = {},
  handleSuccess = () => {},
  handleFail = () => {}
) => {
  return (dispatch) => {
    userService
      .postDangNhap(dataLogin)
      .then((res) => {
        handleSuccess();
        // console.log(res);
        // handleLoginSuccess(res.data.content);
      })
      .catch((err) => {
        handleFail();
        console.log(err);
      });
  };
};
